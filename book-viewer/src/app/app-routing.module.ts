import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { BitcoinComponent } from "./bitcoin/bitcoin.component";
import { HomeComponent } from "./home/home.component";

const routes: Routes = [
  { path: "bitcoin", component: BitcoinComponent },
  { path: "", component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
