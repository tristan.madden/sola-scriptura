import * as p5 from "p5/lib/p5.min.js";

import { Component, OnInit } from "@angular/core";
import { CdkDragDrop, moveItemInArray } from "@angular/cdk/drag-drop";
import { Cross } from ".././Cross";
import { DataService } from ".././data.service";
import { NgForm } from "@angular/forms";
import {
  trigger,
  style,
  transition,
  animate,
  query,
  stagger
} from "@angular/animations";

@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"],
  animations: [
    trigger("stagger", [
      transition("* <=> *", [
        query(
          ":enter",
          [
            style({ opacity: 0, transform: "translateY(-1em)" }),
            stagger(
              "100ms",
              animate(
                "500ms ease-out",
                style({ opacity: 1, transform: "translateY(0em)" })
              )
            )
          ],
          { optional: true }
        )
      ])
    ])
  ]
})
export class HomeComponent implements OnInit {
  suggestedVerses = [
    "James 1:22",
    "Luke 11:28",
    "Matthew 4:4",
    "Matthew 24:35",
    "John 1:1",
    "Proverbs 2:6",
    "Hebrews 4:12",
    "Proverbs 15:14",
    "Hosea 4:6"
  ];

  search: String;
  toolbarExpandAmount: string = "3em";
  tableData = [];
  showPublished: boolean = true;
  showCode: boolean = false;
  showBible: boolean = true;
  showVerse: boolean = true;
  showNote: boolean = false;
  showBackground: boolean = true;
  showQuestions: boolean = false;
  showSettings: boolean = false;
  p5;

  constructor(private data: DataService) {}

  ngOnInit() {
    const myCanvas = <HTMLCanvasElement>(
      document.getElementById("background-animation")
    );
    this.p5 = new p5(this.sketch, myCanvas);
    this.search = this.suggestedVerses[
      Math.floor(Math.random() * this.suggestedVerses.length)
    ];

    if (
      /Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(
        navigator.userAgent
      )
    ) {
      this.showBible = false;
      this.showPublished = true;
      this.showBackground = false;
      this.toolbarExpandAmount = "7em";
    }
  }

  onSubmit(f: NgForm) {
    this.p5.flashScreen();
    //empty-out tableData so different verses aren't stacking in the same array
    this.tableData = Array();
    let foo = f.value.search.split(" ");
    let bar = foo[1].split(":");

    //WARNING: The length of this loop is hard-coded!
    this.data.getVerses(foo[0], bar[0], bar[1]).subscribe((foo: any) => {
      foo.forEach(bar => {
        let mask = {
          bible: bar[0],
          code: bar[1],
          published: bar[2],
          verse: bar[3]
        };
        if (mask.verse) {
          this.tableData.push(mask);
        }
      });
    });
  }

  sketch(p: any) {
    let cross = new Cross(p);
    let backgroundBrightness = 0;
    let flash = false;
    let flashCounter = 0.1;
    let enabled = true;

    p.flashScreen = function() {
      flash = true;
    };

    p.toggleState = function() {
      enabled = !enabled;
      if (enabled) {
        p.loop();
      } else {
        p.background(0);
        p.noLoop();
      }
    };

    cross.init(0, 0, 0);

    p.setup = () => {
      p.createCanvas(p.windowWidth, p.windowHeight, p.WEBGL);
      p.frameRate(60);
    };

    p.draw = function() {
      if (enabled == true) {
        if (flash == true) {
          backgroundBrightness = p.map(Math.sin(flashCounter), 0, 1, 0, 32);
          flashCounter += 0.05;
          //3.236 is two phi
          if (flashCounter >= 3.236) {
            flash = false;
            flashCounter = 0;
          }
        }

        p.background(backgroundBrightness);
        p.directionalLight(20, 20, 20, 1, 0, 0);
        cross.show();
      }
    };

    p.windowResized = function() {
      p.resizeCanvas(p.windowWidth, p.windowHeight);
      p.background(0);
    };
  }

  openQuestions() {
    document.getElementById("questions").style.width = "100vw";
    this.showQuestions = true;
  }

  closeQuestions() {
    document.getElementById("questions").style.width = "0vw";
    this.showQuestions = false;
  }

  toggleSettings() {
    this.showSettings = !this.showSettings;
    if (this.showSettings) {
      document.getElementById("toolbar").style.height = this.toolbarExpandAmount;
    } else {
      document.getElementById("toolbar").style.height = "0";
    }
  }
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.tableData, event.previousIndex, event.currentIndex);
  }
}
