import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})

export class DataService {
  constructor(private http: HttpClient) {}

  url: string = "http://3.16.151.181:8080/scripture";
  //url: string = "http://localhost:8080/scripture";

  /***********************
   * G E T   M A P P I N G
   /**********************/

  getVerses(book, chapter, verse) {
    return this.http.get(this.url + "/" + book + "/" + chapter + "/" + verse);
  }

  getVerse(parent, book, chapter, verse) {
    return this.http.get(
      this.url + "/" + parent + "/" + book + "/" + chapter + "/" + verse
    );
  }

  getLevel(level) {
    return this.http.get(this.url + "/level/" + level);
  }

  getChildren(parentId) {
    let data = this.http.get(this.url + "/children/" + parentId);
    return data;
  }
}
