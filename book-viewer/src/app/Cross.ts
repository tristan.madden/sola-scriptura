export class Cross {
  public x: number;
  public y: number;
  public z: number;
  public p: any;

  constructor(p: any) {
    this.p = p;
  }

  init(rx, ry, rz) {
    this.x = rx;
    this.y = ry;
    this.z = rz;
  }

  show() {
    let unit = this.p.windowHeight * 0.15;
    let halfUnit = unit*0.5;
    let speed = 0.0001;
    let rotation = this.p.frameCount * speed;
    this.p.noStroke();
    this.p.specularMaterial(255);
    this.p.translate(this.x, this.y, this.z);
    this.p.push();
    this.p.rotateZ(rotation);
    this.p.rotateX(rotation);
    this.p.rotateY(rotation);
    this.p.box(unit, unit, halfUnit);
    this.p.translate(0, -unit, 0);
    this.p.box(unit, unit, halfUnit);
    this.p.translate(unit, unit, 0);
    this.p.box(unit, unit, halfUnit);
    this.p.translate(-unit * 2, 0, 0);
    this.p.box(unit, unit, halfUnit);
    this.p.translate(unit, unit, 0);
    this.p.box(unit, unit, halfUnit);
    this.p.translate(0, unit, 0);
    this.p.box(unit, unit, halfUnit);
    this.p.pop();
    this.p.translate(-this.x, -this.y, -this.z);
  }
}
