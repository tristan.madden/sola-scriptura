package tristan.madden.bibles.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import tristan.madden.bibles.entity.Bible;
import tristan.madden.bibles.entity.Verse;
import tristan.madden.bibles.entity.Node;
import tristan.madden.bibles.repository.NodeRepo;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(path = "/scripture")
public class Controller {

    @Autowired
    private NodeRepo repo;

    @GetMapping("/health")
    public String health() {
        return "Hello! There are " + repo.nodeCount() + " entries in the database!";
    }

    @PostMapping(value = "/bible")
    public ResponseEntity<?> postBible(@RequestBody Bible bible) {
        repo.save(bible);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(value = "/verses")
    public ResponseEntity<?> postVerse(@RequestBody VerseWrapper[] wrapped) {

        for (int i = wrapped.length - 1; i >= 0; i--) {
            Verse verse = wrapped[i].getChild();
            verse.setParent(wrapped[i].getParent());
            repo.save(verse);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/{parentId}/{book}/{chapter}/{verse}")
    public Optional<Node> getVerse(@PathVariable("parentId") Long parentId, @PathVariable("book") String book,
            @PathVariable("chapter") Byte chapter, @PathVariable("verse") Byte verse) {
        Optional<Node> query = repo.selectVerse(parentId, book, chapter, verse);
        return query;
    }

    @GetMapping("/{book}/{chapter}/{verse}")
    public List<Object[]> getVerses(@PathVariable("book") String book, @PathVariable("chapter") Byte chapter,
            @PathVariable("verse") Byte verse) {
        List<Object[]> query = repo.selectVerses(book, chapter, verse);
        return query;
    }

    @GetMapping("/chapter/{parentId}/{book}/{chapter}")
    public List<Object> getChapter(@PathVariable("parentId") Long parentId, @PathVariable("book") String book,
            @PathVariable("chapter") Byte chapter) {
        List<Object> query = repo.selectChapter(parentId, book, chapter);
        return query;
    }

}