package tristan.madden.bibles.controller;

import org.springframework.lang.Nullable;

import tristan.madden.bibles.entity.*;

public class VerseWrapper {

    @Nullable
    Verse child;
    @Nullable
    Chapter parent;

    public VerseWrapper(Verse child, Chapter parent) {
        this.child = child;
        this.parent = parent;
    }

    public Verse getChild() {
        return this.child;
    }

    public Chapter getParent() {
        return this.parent;
    }

}
