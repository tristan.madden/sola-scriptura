package tristan.madden.bibles.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import tristan.madden.bibles.entity.Node;

@Repository
public interface NodeRepo extends CrudRepository<Node, Long> {

    List<Node> findByName(Node name);

    List<Node> findByLevel(Long level);

    List<Node> findByParentId(Long id);

    @Query(value = "SELECT * FROM Node n WHERE n.parent_id = ?1 AND n.book = ?2 AND n.chapter = ?3 AND n.verse = ?4", nativeQuery = true)
    Optional<Node> selectVerse(Long parentId, String book, Byte chapter, Byte verse);

    @Query(value = "SELECT a.name, a.code, a.published, b.content FROM Node a INNER JOIN Node b ON (a.id = b.parent_id) WHERE (b.book = ?1 ) AND (b.chapter = ?2 ) AND (b.verse = ?3 ) ORDER BY a.published", nativeQuery = true)
    List<Object[]> selectVerses(String book, Byte chapter, Byte verse);

    @Query(value = "select count(*) from node", nativeQuery = true)
    Long nodeCount();

    @Query(value = "SELECT n.content FROM Node n WHERE n.parent_id = ?1 AND n.book = ?2 AND n.chapter = ?3", nativeQuery = true)
    List<Object> selectChapter(Long parentId, String book, Byte chapter);

}