package tristan.madden.bibles;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BiblesApplication {

	public static void main(String[] args) {
		SpringApplication.run(BiblesApplication.class, args);
	}
}
