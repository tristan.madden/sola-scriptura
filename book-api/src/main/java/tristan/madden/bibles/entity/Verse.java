package tristan.madden.bibles.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Lob;

@DiscriminatorValue("2")
@Entity
public class Verse extends Node {

    Verse() {
    }

    protected String book;
    protected Byte chapter;
    protected Byte verse;
    @Lob
    protected String content;

    public String getBook() {
        return this.book;
    }

    public void setBook(String book) {
        this.book = book;
    }

    public String getContent() {
        return this.content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Byte getChapter() {
        return this.chapter;
    }

    public void setChapter(Byte chapter) {
        this.chapter = chapter;
    }

    public Byte getVerse() {
        return this.verse;
    }

    public void setVerse(Byte verse) {
        this.verse = verse;
    }

}