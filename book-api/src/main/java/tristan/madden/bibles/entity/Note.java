package tristan.madden.bibles.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Lob;

@DiscriminatorValue("3")
@Entity
public class Note extends Node {

    @Lob
    protected String content;

    public String getContent() {
        return this.content;
    }

    void setContent(String content) {
        this.content = content;
    }

}