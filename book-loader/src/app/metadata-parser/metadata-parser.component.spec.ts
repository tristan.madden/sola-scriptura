import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetadataParserComponent } from './metadata-parser.component';

describe('MetadataParserComponent', () => {
  let component: MetadataParserComponent;
  let fixture: ComponentFixture<MetadataParserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetadataParserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetadataParserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
