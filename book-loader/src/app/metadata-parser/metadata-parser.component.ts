import { Component, OnInit } from "@angular/core";
import { Papa } from "ngx-papaparse";
import { HttpClient } from "@angular/common/http";

import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem
} from "@angular/cdk/drag-drop";

@Component({
  selector: "app-metadata-parser",
  templateUrl: "./metadata-parser.component.html",
  styleUrls: ["./metadata-parser.component.scss"]
})
export class MetadataParserComponent implements OnInit {
  isFileLoaded: Boolean = false; //Do not allow a DB operation if there is no file loaded!
  bibles = new Array();
  rejects = new Array();

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }

  constructor(private papa: Papa, private http: HttpClient) {}

  ngOnInit() {}

  changeListener(files: FileList) {
    this.isFileLoaded = true;
    //This is just the configuration for PapaParse!
    let config = {
      delimiter: "", // auto-detect
      newline: "", // auto-detect
      quoteChar: '"',
      escapeChar: '"',
      header: true,
      trimHeaders: false,
      dynamicTyping: false,
      preview: 0,
      encoding: "utf-8",
      worker: false,
      step: undefined,
      complete: (result, file) => {
        this.bibles = result.data;
        console.log(this.bibles);
      },
      error: undefined,
      download: false,
      skipEmptyLines: false,
      chunk: undefined,
      fastMode: undefined,
      beforeFirstChunk: undefined,
      withCredentials: undefined,
      transform: undefined
    };
    /***********************************************
     * This is where the actual parsing takes place!
     ***********************************************/
    Array.from(files).forEach(file => {
      let reader: FileReader = new FileReader();
      reader.readAsText(file);
      reader.onload = e => {
        let csv: string | ArrayBuffer = reader.result;
        this.papa.parse(file, config);
      };
    });
  }

  postWrapper() {
    console.log("postWrapper()");

    let UID = 1;
    while (this.bibles.length > 0) {
      let bible = this.bibles.pop();
      bible.id = UID;
      UID++;

      console.log(bible);
      this.post("http://localhost:8080/scripture/bible", bible);
    }
  }

  post(url, item) {
    this.http.post(url, item).subscribe();
  }
}
