import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { ParserComponent } from "./parser/parser.component";
import { DocumentationComponent } from "./documentation/documentation.component";
import { MetadataParserComponent } from "./metadata-parser/metadata-parser.component";

const routes: Routes = [
  { path: "documentation", component: DocumentationComponent },
  { path: "metadata-parser", component: MetadataParserComponent },
  { path: "parser", component: ParserComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
