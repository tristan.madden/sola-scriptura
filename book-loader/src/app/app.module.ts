import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import {FormsModule} from '@angular/forms';
import { PapaParseModule } from "ngx-papaparse";
import { ParserComponent } from "./parser/parser.component";
import { DocumentationComponent } from "./documentation/documentation.component";
import { NavComponent } from "./nav/nav.component";

//Angular Material
// import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
//import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {DemoMaterialModule} from 'material-module';
import { MetadataParserComponent } from './metadata-parser/metadata-parser.component';
@NgModule({
  declarations: [
    AppComponent,
    ParserComponent,
    DocumentationComponent,
    NavComponent,
    MetadataParserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    PapaParseModule,
    FormsModule,
    // BrowserAnimationsModule,
    DemoMaterialModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
