import { Component, OnInit } from "@angular/core";
import { Papa } from "ngx-papaparse";
import { HttpClient } from "@angular/common/http";
import {
  CdkDragDrop,
  moveItemInArray,
  transferArrayItem
} from "@angular/cdk/drag-drop";

@Component({
  selector: "app-parser",
  templateUrl: "./parser.component.html",
  styleUrls: ["./parser.component.scss"]
})
export class ParserComponent implements OnInit {
  isFileLoaded: Boolean = false; //Do not allow a DB operation if there is no file loaded!
  api: String = "http://localhost:8080/scripture";
  biblesFromServer = new Array();
  allData = new Array();
  rejects = new Array();
  UID: number = 11;

  ngOnInit() {
    this.getBibles();
  }

  constructor(private papa: Papa, private http: HttpClient) {}

  changeListener(files: FileList) {
    this.isFileLoaded = true;
    //This is just the configuration for PapaParse!
    let config = {
      delimiter: "", // auto-detect
      newline: "", // auto-detect
      quoteChar: '"',
      escapeChar: '"',
      header: true,
      trimHeaders: false,
      dynamicTyping: false,
      preview: 0,
      encoding: "utf-8",
      worker: false,
      step: undefined,
      complete: (result, file) => {
        this.allData = result.data;
      },
      error: undefined,
      download: false,
      skipEmptyLines: false,
      chunk: undefined,
      fastMode: undefined,
      beforeFirstChunk: undefined,
      withCredentials: undefined,
      transform: undefined
    };
    /***********************************************
     * This is where the actual parsing takes place!
     ***********************************************/
    Array.from(files).forEach(file => {
      let reader: FileReader = new FileReader();
      reader.readAsText(file);
      reader.onload = e => {
        let csv: string | ArrayBuffer = reader.result;
        this.papa.parse(file, config);
      };
    });
  }

  postVerses() {
    const reg = /\d{1,3}:\d{1,2}/;
    while (this.biblesFromServer.length > 0) {
      let bible = this.biblesFromServer.pop();
      let wholeBible = [];
      for (var i = 0; i < this.allData.length; i++) {
        let s1 = this.allData[i]["Verse"].match(reg);
        let s2 = s1[0].split(":");

        let payload = {
          child: {
            // id: this.UID,
            book: this.allData[i]["Verse"].split(reg)[0].slice(0, -1),
            chapter: parseInt(s2[0]),
            verse: parseInt(s2[1]),
            content: this.allData[i][bible.name]
          },
          parent: bible
        };

        this.UID++;
        wholeBible.push(payload);
      }
      console.log(wholeBible);
      this.post(this.api + "/verses", wholeBible);
    }
  }

  postEverything() {
    const reg = /\d{1,3}:\d{1,2}/;
    while (this.biblesFromServer.length > 0) {
      let parent = this.biblesFromServer.pop();
      let book;
      let chapter;
      let verse;

      let bookID = 0;
      let chapterID = 0;

      for (var i = 0; i < this.allData.length; i++) {
        /* P O S T B O O K S */
        let bookTemp = this.allData[i]["Verse"].split(reg)[0].slice(0, -1);
        if (book != bookTemp) {
          book = bookTemp;
          let child = Object.create(null);
          child.name = book;
          child.id = this.UID;
          let wrapped = Object.create(null);
          wrapped.child = child;
          wrapped.parent = parent;
          // console.log(wrapped);
          this.post(this.api + "/book", wrapped);
          bookID = this.UID;
          this.UID++;
        }
        /* M O R E S P L I T T I N G */
        let s1 = this.allData[i]["Verse"].match(reg);
        let s2 = s1[0].split(":");
        /* P O S T C H A P T E R S */
        let chapterTemp = s2[0];
        if (chapter != chapterTemp) {
          chapter = chapterTemp;
          let child = Object.create(null);
          child.name = chapter;
          child.id = this.UID;
          let parent = Object.create(null);
          parent.id = bookID;
          let wrapped = Object.create(null);
          wrapped.child = child;
          wrapped.parent = parent;
          this.post(this.api + "/chapter", wrapped);
          chapterID = this.UID;
          this.UID++;
        }
        /* P O S T V E R S E S */
        let verseTemp = s2[1];
        let content = this.allData[i][parent.name];
        if (verse != verseTemp) {
          verse = verseTemp;
          let child = Object.create(null);
          child.name = verse;
          child.id = this.UID;
          child.content = content;
          let parent = Object.create(null);
          parent.id = chapterID;
          let wrapped = Object.create(null);
          wrapped.child = child;
          wrapped.parent = parent;
          this.post(this.api + "/verse", wrapped);
          this.UID++;
        }
      }
    }
  }

  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
  getBibles() {
    this.http.get(this.api + "/level/1").subscribe((x: any) => {
      x.forEach(y => {
        this.rejects.push(y);
      });
    });
    console.log("bibles from server", this.biblesFromServer);
  }
  /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

  post(url, item) {
    this.http.post(url, item).subscribe();
  }

  drop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    } else {
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex
      );
    }
  }
}
